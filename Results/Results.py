from Code import Code_bonds
from Code import Code_Stocks
from Code import Investors
import os
import matplotlib.pyplot as plt


#######Graph bonds########

list1 = []
list2 = []
for time in range(2,52):
    STbonds = Code_bonds.Bonds(time, 250)
    x = Code_bonds.STbonds.total_return(STbonds)
    list1.append(x)

for time in range(5, 55):
    LTbonds = Code_bonds.Bonds(time, 1000)
    y = Code_bonds.LTbonds.total_return(LTbonds)
    list2.append(y)

plt.plot(list1)
plt.plot(list2)
plt.xlabel('Year')
plt.ylabel('Amount')
plt.title('Investment bond evolution')
plt.grid(True)
plt.savefig(os.path.abspath('../Results/Investment_Bond_Evolution.png'))
plt.show()

#######Graph stocks########

plt.plot(Code_Stocks.df_FDX["high"])
plt.plot(Code_Stocks.df_Google["high"])
plt.plot(Code_Stocks.df_XOM["high"])
plt.plot(Code_Stocks.df_KO["high"])
plt.plot(Code_Stocks.df_NOK["high"])
plt.plot(Code_Stocks.df_MS["high"])
plt.plot(Code_Stocks.df_IBM["high"])
plt.xlabel('Time')
plt.ylabel('Amount')
plt.title('Stock evolution')
plt.grid(True)
plt.savefig(os.path.abspath('../Results/Investment_Stock_Evolution.png'))
plt.show()

#########Investors########

investor_lambda = Investors.investor(5000)

list_def1 = []
list_def2 = []

for nb in range(500):   # number of ivesstors
    x = Investors.defensive_mode.LT_investment(investor_lambda)
    y = Investors.defensive_mode.ST_investment(investor_lambda)
list_def1.append(x)  # This will store all the 500 results we have in a list
list_def2.append(y)

list_agg = []
for nb in range(500):
    z = Investors.aggressive_mode.return_aggressive(investor_lambda)
list_agg.append(z)

list_mix = []
for nb in range(500):
    a = Investors.mixed_mode.return_mixed(investor_lambda)
list_mix.append(a)
