import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

#####Part 1#####

#Short term bonds
STtermmin = 2
STivt_amount = 250
STinteresty = 0.015

#Long term bonds
LTtermmin = 5
LTivt_amount = 1000
LTinteresty = 0.03

# Computations
class Bonds (object):

    def __init__(self, nb_bond):
        self.nb_bond = nb_bond

    def rate (self, n, amount):
        if 5 > n >= 2:
            if amount >= STivt_amount:
                r = ((1 + STinteresty) ** n) - 1
            else:
                r = ('You must invest a higher amount')
        elif n > 5:
            if amount >= LTivt_amount:
                r = ((1 + LTinteresty) ** n) - 1
            else:
                r = ('You must invest a higher amount')
        else:
            r = ('This is not possible, it must be for a higher period')
        return r

    def total_return (self, n, amount):
        total_return = amount * (1 + self.rate(n, amount))
        return total_return


#Graph
list1 = []
list2 = []
for year in range(50):
    x = STivt_amount*((1+STinteresty)**year)-1
    y = LTivt_amount*((1+LTinteresty)**year)-1
    list1.append(x)
    list2.append(y)

plt.plot(list1)
plt.plot(list2)
plt.xlabel('Year')
plt.ylabel('Amount')
plt.title('Investment evolution')
plt.grid(True)
plt.savefig(os.path.abspath('../Results/Investment_Bond_Evolution.png'))
plt.show()

