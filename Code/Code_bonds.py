class Bonds (object):

    def __init__(self, time, amount):    # define the amount and the time the investor want
        self.time = time
        self.amount = amount

class STbonds (Bonds):

    def total_return(self):
        if self.time >= 2 and self.amount >= 250:            # the short term bond should respect a minimum time of 2 years and amount of 250$
            total_return = self.amount * (((1 + 0.015) ** self.time) - 1)   # computations of the total return using the compounded rate
        else:
            total_return = ('You must invest more than 250$ and for a time superior to 2 years')
        return total_return


class LTbonds (Bonds):

    def total_return (self):
        if self.time > 5 and self.amount >= 1000:          # the long term bond should respect a minimum time of 5 years and amount of 1000$
            total_return = self.amount * (((1 + 0.03) ** self.time) - 1)
        else:
            total_return = ('This might be a short term bond or you should invest more than 1000$')
        return total_return






