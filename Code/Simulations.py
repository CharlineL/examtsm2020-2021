from Code import Code_Stocks
from Code import Code_bonds
from Code import Investors
from Results import Results
import numpy as np
import matplotlib.pyplot as plt
import os

################Yearly average defensive mode#################
investor_lambda = Investors.investor(5000)

list_def1 = []
list_def2 = []
list1 = []
list2 = []
averageLT =[]
averageST = []
for i in range (5):         #number of years
    for nb in range(500):   # number of ivestors
        x = Investors.defensive_mode.LT_investment(investor_lambda)
        y = Investors.defensive_mode.ST_investment(investor_lambda)
        Ltresults = list1.append(x)
        Stresults = list2.append(y)
        averageLT = np.average(Ltresults)
        averageST = np.average(Stresults)
    list_def1.append(averageST)
    list_def2.append(averageLT)

plt.plot(list_def1)
plt.plot(list_def2)
plt.xlabel('Year')
plt.ylabel('Amount')
plt.title('Investment bond evolution')
plt.grid(True)
plt.savefig(os.path.abspath('../Results/Defensive_mode.png'))
plt.show()


####################Yearly average aggressive mode#####################
list_agg = []
list_yearly = []
for i in range (5):
    for nb in range(500):
        z = Investors.aggressive_mode.return_aggressive(investor_lambda)
        agg = list_agg.append(z)
        average_agg = np.average(agg)
    yearly_agg = list_yearly.append(average_agg)

plt.plot(yearly_agg)
plt.xlabel('Year')
plt.ylabel('Amount')
plt.title('Investment bond evolution')
plt.grid(True)
plt.savefig(os.path.abspath('../Results/Aggressive_mode.png'))
plt.show()


#################Yearly average mixed mode#####################
list_mix = []
list_results = []
for i in range (5):
    for nb in range(500):
        a = Investors.mixed_mode.return_mixed(investor_lambda)
        list_mix.append(a)
        avg = np.average(list_mix)
    yearly_mix = list_results.append(avg)

plt.plot(yearly_mix)
plt.xlabel('Year')
plt.ylabel('Amount')
plt.title('Investment bond evolution')
plt.grid(True)
plt.savefig(os.path.abspath('../Results/Mixed_mode.png'))
plt.show()

################ Change in mixed investors ################

def choice_ivt(self):  # Allows to first choose which type on investment it will be
    p = uniform(0, 1)
    if p < 0.75:
        self.choice_ivt = "bonds"
    else:
        self.choice_ivt = "stocks"
    return self.choice_ivt

def return_mixed(self):
    if self.choice_ivt == "stocks":
        return_mixed = aggressive_mode.return_aggressive()
    else:
        return_mixed = defensive_mode.ST_investment() + defensive_mode.LT_investment()
    return return_mixed
