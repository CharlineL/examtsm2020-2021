import pandas as pd
import os
import matplotlib.pyplot as plt
import datetime
from datetime import timedelta
import random

# import data from existing file
FDX = os.path.abspath('../Data/FDX.csv')
df_FDX = pd.read_csv(FDX, sep=",")
Google = os.path.abspath('../Data/GOOGL.csv')
df_Google = pd.read_csv(Google, sep=",")
IBM = os.path.abspath('../Data/IBM.csv')
df_IBM = pd.read_csv(IBM, sep=",")
KO = os.path.abspath('../Data/KO.csv')
df_KO = pd.read_csv(KO, sep=",")
MS = os.path.abspath('../Data/MS.csv')
df_MS = pd.read_csv(MS, sep=",")
NOK = os.path.abspath('../Data/NOK.csv')
df_NOK = pd.read_csv(NOK, sep=",")
XOM = os.path.abspath('../Data/XOM.csv')
df_XOM = pd.read_csv(XOM, sep=",")

possible_stock = [df_XOM, df_NOK, df_MS, df_KO, df_IBM, df_Google, df_FDX]
chosen_stock = possible_stock[random.randint(0,6)]

#Class Stocks
class Stocks(object):
    def __init__(self, Term, Ticker, Amount, StartDate):
        self.Term = Term
        self.Ticker = Ticker
        self.Amount = Amount
        self.StartDate = datetime.strptime(StartDate, "%Y-%m-%d")
        self.EndDate = self.StartDate - timedelta(days=-(self.Term))
        self.Number = round((self.Amount / (self.Ticker.loc[chosen_stock["date"] == self.StartDate, "high"])),4)

    def date(self):
        while self.StartDate not in set(self.Ticker['date']):
            self.StartDate = self.StartDate - timedelta(days=1)
        else:
            self.StartDate = self.StartDate
            return self.StartDate

    def Price(self):
        return self.Ticker.loc[chosen_stock["date"] == self.StartDate, "high"]

    def Return(self):
        PriceBuy = float(self.Ticker.loc[df_XOM["date"] == self.StartDate, "high"]) * self.Number
        PriceSell = float(self.Ticker.loc[df_XOM["date"] == self.EndDate, "high"]) * self.Number
        return round((PriceSell - PriceBuy), 4)

