from Code import Code_bonds
from Code import Code_Stocks
from random import randint
from random import uniform



class investor(object):

    def __init__(self, budget):
        self.budget = budget
        self.nST = randint(2,20)  # the maximum amount a bond can be active is around 20 years. This is the short term bond, time variable.
        self.nLT = randint(5,20)  # This is the long term bond, time variable.

class defensive_mode(investor):

    def LT_investment(self):
        self.amount = 0.5 * self.budget
        self.time = self.nLT
        self.y = self.amount // 1000                          #Determine the number of LT bonds
        returnLTbonds = self.y * Code_bonds.LTbonds.total_return(self)
        self.remaining_budget = self.budget - (self.y * 1000)  # Remaining budget for ST bonds
        return returnLTbonds

    def ST_investment(self):
        self.time = self.nST
        self.amount = self.remaining_budget
        x = self.amount // 250
        returnSTbonds = x * Code_bonds.STbonds.total_return(self)
        return returnSTbonds

class aggressive_mode(investor):

    def return_aggressive(self):
        while self.budget > 100:
            x = Code_Stocks.Stocks.Return(self)
        return x

class mixed_mode(investor):

    def choice_ivt(self):    # Allows to first choose which type on investment it will be
        p = uniform(0,1)
        if p < 0.25:
            self.choice_ivt = "bonds"
        else:
            self.choice_ivt = "stocks"
        return self.choice_ivt

    def return_mixed(self):
        if self.choice_ivt == "stocks":
            return_mixed = aggressive_mode.return_aggressive()
        else:
            return_mixed = defensive_mode.ST_investment() + defensive_mode.LT_investment()
        return return_mixed
